<?php

require_once(dirname(__FILE__) . '/flexihash.php');
require_once(ABSPATH . 'wp-config.php');

global $r2db_hash_map;
$r2db_hash_map = new Flexihash(null, R2DB_FLEXIHASH_REPLICAS);

/* Hashing function to map blogs to databases.
 *
 * Implements a consistent hashing scheme using Flexihash.
 */
function noblogs_db_callback($query, $wpdb) {
  global $r2db_hash_map;
  if (preg_match("/^{$wpdb->base_prefix}(\d+)_/", $wpdb->table, $matches)) {
    $blog_id = $matches[1];
    return $r2db_hash_map->lookup($blog_id);
  }
}

$wpdb->add_callback('noblogs_db_callback');

/* Load backend shards and add them to FlexiHash / $wpdb. */
function noblogs_load_backends() {
  global $wpdb;
  global $r2db_hash_map;
  global $noblogs_config;
  $backend_map = array();
  foreach ($noblogs_config['db_config']['backends'] as $backend_name => $backend) {
    $b = array(
       "host" => $backend['host'] . ":" . $backend['port'],
       "user" => $backend['user'],
       "password" => $backend['password'],
       "name" => $backend['name'],
       "dataset" => $backend_name,
       "read" => 1,
       "write" => 1,
       "timeout" => 10
    );
    $wpdb->add_database($b);
    $r2db_hash_map->addTarget($backend_name);
    $backend_map[$backend_name] = $b;
  }
  return $backend_map;
}

/* Load the local and master backends to $wpdb. */
function noblogs_load_global_dataset() {
  global $wpdb;
  global $noblogs_config;
  $master = $noblogs_config['db_config']['master'];
  // IP adress of the local mysql instance with the 'master' db.
  // We want to override this in certain testing scenarios, to use
  // a mysql server not on localhost.
  $local_master_host = '127.0.0.1';
  if (isset($master['local_host'])) {
    $local_master_host = $master['local_host'];
  }
  if ($noblogs_config['db_config']['is_master']) {
    $wpdb->add_database(array(
       "host" => $local_master_host . ":" . $master['port'],
       "user" => $master['user'],
       "password" => $master['password'],
       "name" => $master['name'],
       "dataset" => "global",
       "read" => 1,
       "write" => 1,
       "timeout" => 2
    ));
  } else {
    $wpdb->add_database(array(
       "host" => $master['host'] . ":" . $master['port'],
       "user" => $master['user'],
       "password" => $master['password'],
       "name" => $master['name'],
       "dataset" => "global",
       "read" => 0,
       "write" => 1,
       "timeout" => 2
    ));
    $wpdb->add_database(array(
       "host" => $local_master_host . ":" . $master['port'],
       "user" => $master['user'],
       "password" => $master['password'],
       "name" => $master['name'],
       "dataset" => "global",
       "read" => 1,
       "write" => 0,
       "timeout" => 2
    ));
  }
}
