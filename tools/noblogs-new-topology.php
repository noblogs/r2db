#!/opt/noblogs/cron/php-noblogs
<?php
/*
   Parse command line options
 */
$opts = getopt('',array('no-database', 'calc-size', 'db-only'));
if (array_key_exists('calc-size', $opts)) {
    $opts['no-database'] = true;
    $g_added_size = array();
}
$new_topology = array_pop($argv);
$old_topology = array_pop($argv);
if (!($new_topology && $old_topology)) {
    usage();
    exit(-1);
}

// Get all blogs
define('WP_CACHE',false);
require_once('wp-load.php');
require_once('db-config.php');
$blogs = get_all_blogs();

$n_hashmap = new Flexihash();
$new_map = noblogs_load_backends($new_topology, $n_hashmap);

$o_hashmap = new Flexihash();
$old_map = noblogs_load_backends($old_topology, $o_hashmap);



foreach ($blogs as $blog) {
    $blog_id = $blog->blog_id;

    $old_params = fhash($blog_id, $old_map, $o_hashmap);
    $old_dburi = mysqlurl($old_params);
    $new_params = fhash($blog_id, $new_map, $n_hashmap);
    $new_dburi = mysqlurl($new_params);

    if ($new_counts[$new_params['host']]) {
        $new_counts[$new_params['host']] += 1;
    } else {
        $new_counts[$new_params['host']] = 1;
    }

    if ($old_dburi != $new_dburi) {
        echo "echo moving blog $blog_id from " . $old_params['host'] . " to " . $new_params['host'] . "\n";
        if ( !array_key_exists('no-database', $opts) ) {
            echo "tables=\$(mysql " . mysqlopts($old_params) . " " . $old_params['db'] . " -NBe \"show tables like 'wp\\_" . $blog_id . "\\_%'\")\n";
            echo "mysqldump --opt " . mysqlopts($old_params) . " " . $old_params['db'] . " \${tables} \\\n";
            echo "  | mysql " . mysqlopts($new_params) . " " . $new_params['db'] . "\n";
        }
	if (!array_key_exists('db-only',$opts)) {
        if (array_key_exists('calc-size', $opts)) {
#            $cmd = sprintf("ssh root@%s du -sk /opt/noblogs/www/wp-content/blogs.dir/%d", $old_params['host'], $blog_id);
	    $cmd = sprintf("du -sk /opt/noblogs/www/wp-content/blogs.dir/%d", $blog_id);
	    list($size, $dummy) = explode("\t",exec($cmd, $ret));
            $g_added_size[$new_params['host']] += $size;
        } else {
            printf("ssh root@%s rsync -avz --delete /opt/noblogs/www/wp-content/blogs.dir/%d root@%s:/opt/noblogs/www/wp-content/blogs.dir/\n", $old_params['host'], $blog_id, $new_params['host']);
        }
	}
        $moved_count += 1;
    } else {
        echo "echo blog $blog_id stays on " . $old_params['host'] . "\n";
    }
}


echo "\n\n\nBlog distribution:\n";
print_r($new_counts);
echo "\n $moved_count blogs moved\n";

if (array_key_exists('calc-size', $opts)) {
    echo "Variations in disk space for hosts (kB):\n";
    print_r($g_added_size);
}

function fhash($dbid, $reversemap, $hashptr) {
  $lookup = $hashptr->lookup($dbid);
  $backend = $reversemap[$lookup];
  $result = array();
  if (preg_match('/^(.*):([0-9]*)$/', $backend['host'], $matches)) {
    $result['host'] = $matches[1];
    $result['port'] = $matches[2];
  }
  $result['user'] = $backend['user'];
  $result['password'] = $backend['password'];
  $result['db'] = $backend['name'];
  return $result;
}

function mysqlopts(&$attrs) {
  return ("-h" . $attrs['host'] . " -P" . $attrs['port'] . " -u" . $attrs['user']
	  . " '-p" . $attrs['password'] . "'");
}

function mysqlurl(&$attrs) {
  return ("mysql://" . $attrs['user'] . "@" . $attrs['host']
	  . ":" . $attrs['port'] . "/" . $attrs['db']);
}

function get_all_blogs() 
{
  global $wpdb;
  $sql = "SELECT blog_id, domain, path FROM $wpdb->blogs WHERE deleted = 0 AND archived = '0' ORDER BY domain, path";
  $result = $wpdb->get_results($sql);
  return ($result);
}

function usage() {
    $str = <<<USAGE
noblogs-new-topology.php [--no-database|--calc-size] <OLD_MAP> <NEW_MAP> 
USAGE;
    echo $str;
}
